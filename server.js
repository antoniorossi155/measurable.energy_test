const express = require("express");
var logger = require("morgan");

const sensorRoute = require("./routes/sensor");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/sensors", sensorRoute);

const server = app.listen(8080, function () {
  console.log("Ready on port %d", server.address().port);
});

module.exports = server;
