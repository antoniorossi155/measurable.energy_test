const fs = require("fs");
const path = require("path");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const csv = require("csv-parser");

const parseCsv = (path) => {
  return new Promise((resolve, reject) => {
    const data = [];
    fs.createReadStream(path)
      .pipe(csv())
      .on("data", (row) => {
        data.push(row);
      })
      .on("end", () => {
        console.log("CSV file successfully processed");
        resolve(data);
      })
      .on("error", reject);
  });
};

const writeCsv = async (data, path) => {
  const csvWriter = createCsvWriter({
    path,
    header: [
      { id: "date", title: "date" },
      { id: "start_time", title: "start_time" },
      { id: "timesteps", title: "timesteps" },
      { id: "sensor_value", title: "sensor_value" },
    ],
  });

  return await csvWriter.writeRecords(data);
};

const parseArray = (arrayString) => {
  const noBracketsString = arrayString.substring(1, arrayString.length - 1);
  return noBracketsString.split(",").map((element) => parseInt(element, 10));
};

const formatDataFromCsv = (data) => {
  const formattedData = data.map((element) => {
    return {
      date: parseInt(element.date),
      start_time: parseInt(element.start_time),
      timesteps: parseArray(element.timesteps),
      sensor_value: parseArray(element.sensor_value),
    };
  });
  return formattedData;
};

const arrayToString = (array) => JSON.stringify(array);

exports.postData = async (req, res) => {
  try {
    const { date, start_date, timesteps, sensor_value } = req.body;
    const timestepsString = arrayToString(timesteps);
    const sensorValueString = arrayToString(sensor_value);

    const dataPath = path.resolve(__dirname, "../data/data.csv");

    const data = await parseCsv(dataPath);
    const dataToWrite = [
      ...data,
      {
        date: date.toString(),
        start_time: start_date.toString(),
        timesteps: timestepsString,
        sensor_value: sensorValueString,
      },
    ];
    await writeCsv(dataToWrite, dataPath);

    res.status(200).json({ message: "Data correctly added to csv file" });
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
};

exports.getAllData = async (req, res, next) => {
  try {
    const dataPath = path.resolve(__dirname, "../data/data.csv");
    const csvData = await parseCsv(dataPath);

    const data = await formatDataFromCsv(csvData);
    debugger;

    res
      .status(200)
      .json({ message: "Data fetched correctly from csv file", data });
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
};

// Accepts data as follow: /sensors/query?date=200222&time=134141
exports.getQueryData = async (req, res, next) => {
  const { date, time } = req.query;

  const dataPath = path.resolve(__dirname, "../data/data.csv");
  const csvData = await parseCsv(dataPath);
  const data = await formatDataFromCsv(csvData);

  const result = data.find(
    (element) =>
      element.date === parseInt(date, 10) &&
      element.start_time === parseInt(time, 10)
  );

  res.status(200).json({ message: "Data fetch correctly", result });
};
