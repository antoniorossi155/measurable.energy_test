const express = require("express");
const sensorController = require("../controllers/sensor");

const router = express.Router();

router.get("/", sensorController.getAllData);
router.get("/query", sensorController.getQueryData);
router.post("/", sensorController.postData);

module.exports = router;
