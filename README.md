# Measurable.energy Test

## How to run

To run the server:
`npm start`

To run the test:
`npm run test`

## How to use

`GET /sensors` returns all the data on the CSV file, properly formatted to fed to a front-end

`GET /sensors/query` returns the result from the query. E.g. `localhost:8080/sensors/query?date=200222&time=134141` if run in local

`POST /sensors` to be used to add data to the CSV in the following format

```
{
    "date": 211118,
    "start_date": 152715,
    "timesteps": [1,2,3,4],
    "sensor_value": [0.12,0.45,0.250]
}
```

The test suite evaluate two `GET` endpoints (I was running out of time to test all of them).

## Notes

The requirement were not specified on the instructions provided.

The instructions were the following:

1. A simple API which consumes a POST request and a GET request
2. The POST request should ingest data in the following format: `see above`
3. You will be provided with a csv containing four columns date, start_time,
   timesteps, and sensor_value. Date the date of data collection in the
   following format `%yy%mm%dd` and start_time is in the following format
   `%HH%MM%SS`. Timesteps and sensor_value are simply lists containing their
   corresponding values.
4. (_BONUS CHALLENGE_) this is not mandatory to complete, but impress us if
   you can develop a UI to show allow the uploading of a csv and return the
   results from a query using date/ start_time.

To summarise, the requirement were to create an API consuming a `POST` and a `GET` request. However, it's not specified the aim of the two requests.

What was supposed to return the `GET` request?
What was supposed to do the `POST` request?

I had to guess, so I ended up returning from the `GET /sensors` request the value of the CSV as a JSON object properly formatted to be fed to a front-end

For the `POST /sensors` request, I'm adding the body of the request to the to the CSV

As it wasn't mentioned in the assignment, but reading between the lines of the Bonus Challenge, I created another `GET /sensors/query` endpoint that queries the CSV and return the requested value.

## About the Bonus Challenge

As I had to spend a considerable amount of time trying to understand (and at the end guessing) the requirement of the tech test, I haven't had the time to do much of it.

I created the `GET /sensors/query` that would have allowed me to return the result of the user's query.

My way of approcing the task would have been the follow:

1. create and `POST` endpoint to receive the CSV and using `multer.diskStorage({destination: ..., filename: ...)}` to save it.
2. Create a view for the page with an upload button for the CSV pointing to the aforementioned endpoint
3. Once the CSV was uploaded successfully, I would have displayed a date and time picker to allow the user to query it and I would have used the `GET /sensors/query` endpoint to return the required data.
