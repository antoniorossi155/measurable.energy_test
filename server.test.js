const request = require("supertest");
const app = require("./server");

describe("Sensors API", () => {
  describe("given that GET /sensor receive a request", () => {
    it("should return all records from the csv file", () => {
      return request(app)
        .get("/sensors/")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).toEqual(
            expect.objectContaining({
              data: expect.arrayContaining([
                expect.objectContaining({ date: expect.any(Number) }),
                expect.objectContaining({ start_time: expect.any(Number) }),
                expect.objectContaining({ timesteps: expect.any(Array) }),
                expect.objectContaining({ sensor_value: expect.any(Array) }),
              ]),
            })
          );
        });
    });
  });
  describe("given that GET /sensor/query receive a request", () => {
    it("should return all records from the csv file", () => {
      return request(app)
        .get("/sensors/query?date=200222&time=134141")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((response) => {
          const result = response.body.result;
          expect(response.body).toEqual(
            expect.objectContaining({
              result: expect.objectContaining({
                date: expect.any(Number),
                start_time: expect.any(Number),
                timesteps: expect.any(Array),
                sensor_value: expect.any(Array),
              }),
            })
          );
          expect(result.date).toEqual(200222);
          expect(result.start_time).toEqual(134141);
        });
    });
  });
});
